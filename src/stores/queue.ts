import { ref } from "vue";
import { defineStore } from "pinia";
import type Queue from "@/types/Queue";
import queueService from "@/service/queue";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import orderService from "@/service/order";

export const useQueueStore = defineStore("queue", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const queues = ref<Queue[]>([]);
  // const queuesList = ref<Queue[]>([]);

  async function getQueues() {
    loadingStore.isLoading = true;
    try {
      const res = await queueService.getQueues();
      queues.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get queue information from database.");
    }
    loadingStore.isLoading = false;
  }

  async function getQueueById(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = (await queueService.getQueueById(id)).data;
      return res;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get menu information from database.");
    }
    loadingStore.isLoading = false;
  }

  const updateQueue = async (queue: Queue) => {
    loadingStore.isLoading = true;
    try {
      const res = await queueService.updateQueue(queue);
      console.log(res.data);
      await getQueues();
      return res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  const deleteQueue = async (queue: Queue): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      await updateQueue(queue);
      const res = await queueService.deleteQueue(queue.id!);
      await orderService.deleteOrderItem(queue.orderItems?.id!);
    } catch (e) {
      console.log(e);
    }
    await getQueues();
    loadingStore.isLoading = false;
  };

  const createQueue = async (queue: Queue) => {
    loadingStore.isLoading = true;
    try {
      console.log(queue);
      const res = await queueService.saveQueue(queue);
      return res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  return {
    queues,
    getQueues,
    // queuesList,
    updateQueue,
    getQueueById,
    deleteQueue,
    createQueue,
  };
});
