import { defineStore } from "pinia";
import { useMessageStore } from "./message";
import auth from "@/service/auth";
import router from "@/router";
import { ref } from "vue";
import { useLoadingStore } from "./loading";

export const useAuthStore = defineStore("auth", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const id = ref();
  const email = ref();
  const name = ref();
  const role = ref();
  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      id.value = localStorage.getItem("id");
      email.value = localStorage.getItem("email");
      name.value = localStorage.getItem("name");
      role.value = localStorage.getItem("role");
      return true;
    }
    return false;
  };

  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      console.log(res);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      localStorage.setItem("id", res.data.user.id);
      localStorage.setItem("name", res.data.user.name);
      localStorage.setItem("email", username + "@gmail.com");
      localStorage.setItem("role", res.data.user.role);
      if (res.data.user.role === "chef") {
        router.push("/chefview");
      } else if (res.data.user.role === "waiter") {
        router.push("/serveview");
      } else {
        router.push("/");
      }
    } catch (e) {
      messageStore.showMessage("Login or password is incorrect.");
    }
    loadingStore.isLoading = false;
  };

  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    localStorage.removeItem("email");
    localStorage.removeItem("name");
    localStorage.removeItem("role");
    localStorage.removeItem("id");
    email.value = "";
    name.value = "";
    role.value = "";
    id.value = "";
    console.log(localStorage.removeItem("token"));
    router.replace("/login");
  };

  return { isLogin, login, logout, email, name, role, id };
});
