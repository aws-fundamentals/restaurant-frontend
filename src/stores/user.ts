import { ref } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/service/user";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useUserStore = defineStore("user", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get user information from database.");
    }
    loadingStore.isLoading = false;
  }
  const editedUser = ref<User>({
    username: "",
    password: "",
    name: "",
    role: "",
    phone: "",
    salary: 0,
  });

  const deleteUser = async (id: number): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);
      console.log(res);
    } catch (e) {
      console.log(e);
    }
    await getUsers();
    loadingStore.isLoading = false;
  };

  const saveUser = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        //edt id not empty = edt
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
        console.log(res);
      } else {
        //new
        const res = await userService.saveUser(editedUser.value);
        console.log(res);
      }
    } catch (e) {
      console.log(e);
    }
    await getUsers();
    dialog.value = false;
    clear();
    loadingStore.isLoading = false;
  };

  const clear = () => {
    editedUser.value = {
      username: "",
      password: "",
      name: "",
      role: "",
      phone: "",
      salary: 0,
    };
  };

  const editUser = (user: User) => {
    dialog.value = true;
    editedUser.value = { ...user }; //new object
    //JSON.parse(JSON.stringify(user));
  };

  return {
    users,
    deleteUser,
    dialog,
    editedUser,
    clear,
    saveUser,
    editUser,
    // login,
    getUsers,
  };
});
