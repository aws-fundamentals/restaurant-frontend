import { reactive, ref } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import orderService from "@/service/order";
import type Menu from "@/types/Menu";
import type orderItem from "@/types/OrderItem";
import { useQueueStore } from "./queue";
import { useLoadingStore } from "./loading";

export const useCartStore = defineStore("cart", () => {
  const queueStore = useQueueStore();
  const loadingStore = useLoadingStore();
  let cartList = reactive<orderItem[]>([]);
  let totalPrice = 0;

  async function getTotalPrice(orderId: string) {
    totalPrice = 0;
    const res = (await orderService.getOrderById(orderId)).data[0].orderItems;
    for (const item of res) {
      totalPrice += item.qty * item.product.price;
    }
    return totalPrice;
  }

  async function addCart(pd: Menu[], orderId: string) {
    for (const item of pd) {
      const orderItem: orderItem = {
        name: item.name,
        qty: item.qty!,
        product: item,
      };
      totalPrice += item.qty! * item.price;
      cartList.push(orderItem);
    }
    addOrderItems(orderId, cartList);
  }

  function clearCart() {
    cartList = [];
    totalPrice = 0;
  }

  async function addOrderItems(orderId: string, cartList: orderItem[]) {
    loadingStore.isLoading = true;
    try {
      const table = ref();
      const res = await orderService.getOrderById(orderId);
      table.value = res.data;
      const order: Order = {
        id: orderId,
        orderItems: cartList,
        table: table.value.table_number,
        table_number: table.value.table_number,
      };
      // console.log(order);
      const temp = await orderService.updateOrder(orderId, order);
      const oldOrder = ref<Order>();
      oldOrder.value = temp.data;
      const rangeOrder = parseInt(oldOrder.value?.orderItems.length + "");
      console.log(oldOrder.value);

      console.log(cartList);
      const cartListLength = cartList.length;

      for (let item = rangeOrder - cartListLength; item < rangeOrder; item++) {
        console.log("orderItem id " + item);
        for (
          let i =
            parseInt(oldOrder.value?.orderItems[item].qty + "") -
            cartList[item].qty;
          i < parseInt(oldOrder.value?.orderItems[item].qty + "");
          i++
        ) {
          // console.log("queue ตัวที่ " + i);
          const queue = await queueStore.createQueue({
            name: oldOrder.value?.orderItems![item].name + "",
            status: "waiting",
            tableId: oldOrder.value?.table_number,
            orderItemsId: oldOrder.value?.orderItems![item].id,
          });
          // console.log(queue);
        }
      }
    } catch (e) {
      console.log(e);
    }
    clearCart();
    loadingStore.isLoading = false;
  }

  return {
    cartList,
    addCart,
    clearCart,
    addOrderItems,
    totalPrice,
    getTotalPrice,
  };
});
