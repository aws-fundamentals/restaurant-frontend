import { defineStore } from "pinia";
import receiptService from "@/service/receipt";
import type Receipt from "@/types/Receipt";
import { useLoadingStore } from "./loading";

export const useRecieptStore = defineStore("receipt", () => {
  const loadingStore = useLoadingStore();
  const createReceipt = async (receipt: Receipt) => {
    loadingStore.isLoading = true;
    try {
      const res = await receiptService.saveReceipt(receipt);
      return res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  return {
    createReceipt,
  };
});
