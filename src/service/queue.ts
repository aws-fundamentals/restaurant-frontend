import http from "./axios";
import type Queue from "@/types/Queue";

function getQueues() {
  return http.get("/queues");
}

function getQueueById(id: number) {
  return http.get(`/queues/${id}`);
}

function saveQueue(queue: Queue) {
  return http.post("/queues", queue);
}

function updateQueue(queue: Queue) {
  return http.patch(`/queues/${queue.id}`, queue);
}

function deleteQueue(id: number) {
  return http.delete(`/queues/${id}`);
}

export default { getQueues, saveQueue, updateQueue, deleteQueue, getQueueById };
