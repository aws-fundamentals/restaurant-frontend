import type Menu from "./Menu";
import type Order from "./Order";

export default interface orderItem {
  id?: number;
  name: string;
  qty: number;
  product?: Menu;
  orders?: Order;
}
