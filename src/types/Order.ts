import type orderItem from "./OrderItem";
import type Receipt from "./Receipt";
import type Table from "./Table";

export default interface order {
  id?: string;
  table_number: number;
  table: Table;
  orderItems: orderItem[];
  receipt?: Receipt[];
  employeeId?: number;
}
