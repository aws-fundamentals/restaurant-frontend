import type orderItem from "./OrderItem";
import type Table from "./Table";
import type User from "./User";

export default interface Queue {
  id?: number;
  name: string;
  status: string;
  tableId?: number;
  orderItemsId?: number;
  employeeId?: number;
  employee?: User;
  table?: Table;
  orderItems?: orderItem;
}
